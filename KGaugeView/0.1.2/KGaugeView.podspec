Pod::Spec.new do |s|
  s.name             = "KGaugeView"
  s.version          = "0.1.2"
  s.summary          = "KGaugeView is a complex Gauge that can be configured simply."
  s.homepage         = "https://rahmadeyad@bitbucket.org/rahmadeyad/kgaugeview.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'rahmadeyadg' => 'rahmadeyadg@gmail.com' }
  s.source           = { :git => "https://rahmadeyad@bitbucket.org/rahmadeyad/kgaugeview.git", :tag => s.version }

  s.platform     = :ios, '13.0'

  s.ios.deployment_target = '13.0'
  s.swift_version = '5.0'
  s.source_files = 'EGaugeView/Classes/**/*'
end
