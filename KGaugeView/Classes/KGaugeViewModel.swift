//
//  KGaugeViewModel.swift
//  KGaugeView
//
//  Created by Ahmad Eyad on 29/04/2021.
//

import SwiftUI

public class KGaugeViewModel: ObservableObject {
    
    public init() {}
    
    @Published public var list: [KGaugeModel] = [
        KGaugeModel(score: 0.6, systemIconName: "bed.double.fill"),
        KGaugeModel(score: 0.4, systemIconName: "heart.fill"),
        KGaugeModel(score: 0.8, systemIconName: "paintbrush.pointed.fill"),
        KGaugeModel(score: 0.7, systemIconName: "externaldrive.fill"),
        KGaugeModel(score: 0.4, systemIconName: "doc.text.below.ecg.fill"),
        KGaugeModel(score: 0.6, systemIconName: "bed.double.fill"),
        KGaugeModel(score: 0, systemIconName: "paintbrush.pointed.fill"),
        KGaugeModel(score: 1, systemIconName: "heart.fill"),
        KGaugeModel(score: 0.4, systemIconName: "externaldrive.fill"),
        KGaugeModel(score: 0.8, systemIconName: "bed.double.fill")
    ]
    @Published public var totalAngle: Double = 250
    @Published public var title: String = "My Lifestyle"
    @Published public var levelColors: [Color] = [Color]()
    @Published public var levelsCount: Double = 5
    @Published public var duration: Double = 1
    @Published public var size: CGFloat = 300
    @Published public var count: CGFloat = 10
    
    func changeTotalAngle(totalAngle: Double) {
        self.totalAngle = totalAngle
    }
    
    func changeTitle(title: String) {
        self.title = title
    }
    
    func changeLevelCount(count: Double) {
        self.levelsCount = count
    }
    
    func changeDuration(duration: Double) {
        self.duration = duration
    }
    
    func changeSize(size: CGFloat) {
        self.size = size
    }
}

protocol TitleViewModel {
    var title: String { get set }
}

protocol ChangeSize {
    var size: CGFloat { get set }
}
