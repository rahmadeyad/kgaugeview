//
//  KGaugeView.swift
//  KGaugeView
//
//  Created by Ahmad Eyad on 29/04/2021.
//

import SwiftUI

public struct KGaugeView: View {
    
    @ObservedObject var viewModel: KGaugeViewModel
    @State var id = UUID()
    
    public init(viewModel: KGaugeViewModel) {
        self.viewModel = viewModel
    }
    
    public init(title: String, list: [KGaugeModel], totalAngle: Double = 250, levelsCount: Double = 5, animationDuration: Double = 1, size: CGFloat = 250, levelColors: [Color] = [Color]()) {
        self.viewModel = KGaugeViewModel()
        self.viewModel.title = title
        self.viewModel.list = list
        self.viewModel.totalAngle = totalAngle
        self.viewModel.levelsCount = levelsCount
        self.viewModel.duration = animationDuration
        self.viewModel.size = size
        self.viewModel.levelColors = levelColors
        self.viewModel.count = CGFloat(list.count)
    }
    
    public var body: some View {
        if #available(iOS 14.0, *) {
            ZStack {
                ForEach(0..<Int(viewModel.count)) { i in
                    DashBoardItem(viewModel: viewModel, levelScore: viewModel.list[i].score, startAngle: startAngle(index: i), endAngle: endAngle(index: i), index: i)
                }
                VStack {
                    Spacer()
                    Text(viewModel.title)
                        .bold()
                        .font(.custom("", size: viewModel.size/12))
                        .padding(.bottom, viewModel.size/8)
                }
                .frame(width: viewModel.size, height: viewModel.size)
            }
            .frame(width: viewModel.size, height: viewModel.size)
            .id(id)
            .onChange(of: viewModel.levelsCount) { (value) in
                id = UUID()
            }
            .onChange(of: viewModel.size) { (value) in
                id = UUID()
            }
            .onChange(of: viewModel.duration) { (value) in
                id = UUID()
            }
            .onChange(of: viewModel.count) { (value) in
                id = UUID()
            }
        } else {
            // Fallback on earlier versions
            ZStack {
                ForEach(0..<Int(viewModel.count)) { i in
                    DashBoardItem(viewModel: viewModel, levelScore: viewModel.list[i].score, startAngle: startAngle(index: i), endAngle: endAngle(index: i), index: i)
                }
                VStack {
                    Spacer()
                    Text(viewModel.title)
                        .bold()
                        .font(.custom("", size: viewModel.size/12))
                        .padding(.bottom, viewModel.size/8)
                }
                .frame(width: viewModel.size, height: viewModel.size)
            }
            .frame(width: viewModel.size, height: viewModel.size)
            .id(id)
        }
    }
    
    func startAngle(index: Int) -> Double {
        if index == 0 {
            return (((360 - viewModel.totalAngle)/2)+90) + 2.5
        }
        else {
            var temp: Double = ((360 - viewModel.totalAngle)/2)+90
            for _ in 0...index-1 {
                temp += viewModel.totalAngle / Double(viewModel.count)
            }
            return temp + 2.5
        }
    }
    
    func endAngle(index: Int) -> Double {
        if index == Int(viewModel.count) {
            return 90-((360-viewModel.totalAngle)/2)
        }
        else {
            var temp: Double = ((360 - viewModel.totalAngle)/2)+90
            for _ in 0...index {
                temp += viewModel.totalAngle / Double(viewModel.count)
            }
            return temp
        }
    }
}
