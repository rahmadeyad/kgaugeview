//
//  DashBoardItem.swift
//  KGaugeView
//
//  Created by Ahmad Eyad on 29/04/2021.
//

import SwiftUI

public struct DashBoardItem: View {
    
    @ObservedObject var viewModel: KGaugeViewModel
    @State var levelScore: Double = 0
    var startAngle: Double = 0
    var endAngle: Double = 0
    @State var currentScore = 0
    var index: Int = 0
    
    public var body: some View {
        ZStack {
            ForEach(0..<Int(viewModel.levelsCount)) { i in
                KGaugeShape(startAngle: .degrees(startAngle), endAngle: .degrees(endAngle), clockwise: true)
                    .strokeBorder(i < currentScore ? getColor(withOpacity: 0) : getColor(withOpacity: i).opacity(0.4), lineWidth: lineSize())
                    .frame(width: getFrame(i: i), height: getFrame(i: i))
            }
            .padding(viewModel.size/8)
            VStack {
                if viewModel.list[index].iconName != nil {
                    Image(viewModel.list[index].iconName!)
                        .resizable()
                        .foregroundColor(getColor(withOpacity: 0))
                        .frame(width: viewModel.size/12, height: viewModel.size/12)
                        .rotationEffect(.degrees(-(-(viewModel.totalAngle/2) + 1.25 + (viewModel.totalAngle / Double(viewModel.count*2)) + (viewModel.totalAngle / Double(viewModel.count)) * Double(index))))
                }
                else {
                    Image(systemName: viewModel.list[index].systemIconName!)
                        .resizable()
                        .foregroundColor(getColor(withOpacity: 0))
                        .frame(width: viewModel.size/12, height: viewModel.size/12)
                        .rotationEffect(.degrees(-(-(viewModel.totalAngle/2) + 1.25 + (viewModel.totalAngle / Double(viewModel.count*2)) + (viewModel.totalAngle / Double(viewModel.count)) * Double(index))))
                }
                Spacer()
            }
            .frame(maxHeight: .infinity, alignment: .top)
            .rotationEffect(.degrees(-(viewModel.totalAngle/2) + 1.25 + (viewModel.totalAngle / Double(viewModel.count*2)) + (viewModel.totalAngle / Double(viewModel.count)) * Double(index)))
        }
        .onAppear {
            animation()
        }
    }
    
    func animation() {
        levelScore = levelScore*Double(viewModel.levelsCount)
        Timer.scheduledTimer(withTimeInterval: viewModel.duration/Double(viewModel.levelsCount), repeats: true) { timer in
            if levelScore < 1 {
                timer.invalidate()
            }
            else {
                currentScore += 1
                levelScore -= 1
            }
        }
    }
    
    func getColor(withOpacity: Int) -> Color {
        switch withOpacity {
        case 0:
            if currentScore == 0 {
                return viewModel.levelColors.count > 0 ? viewModel.levelColors[0] : Color.init("Color-1")
            }
            return viewModel.levelColors.count > 0 ? viewModel.levelColors[currentScore - 1] : Color.init("Color-\(currentScore)")
        default:
            return viewModel.levelColors.count > 0 ? viewModel.levelColors[withOpacity] : Color.init("Color-\(withOpacity + 1)")
        }
    }
    
    func getFrame(i: Int) -> CGFloat {
        var temp: CGFloat = viewModel.size - (viewModel.size/8) - (space()*CGFloat(viewModel.levelsCount)-1)
        temp += (space()*CGFloat(i))
        return temp
    }
    
    func space() -> CGFloat {
        return viewModel.size/11.6
    }
    
    func lineSize() -> CGFloat {
        return viewModel.size/35
    }
}
