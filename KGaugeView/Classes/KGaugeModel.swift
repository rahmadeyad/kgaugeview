//
//  KGaugeModel.swift
//  KGaugeView
//
//  Created by Ahmad Eyad on 29/04/2021.
//

import SwiftUI

public struct KGaugeModel: Identifiable {
    public var id: UUID?
    public var score: Double
    public var systemIconName: String?
    public var iconName: String?

    public init(score: Double, systemIconName iconFromSfSymbol: String) {
        self.score = score
        self.systemIconName = iconFromSfSymbol
    }
    
    public init(score: Double, iconName iconFromYourAssets: String) {
        self.score = score
        self.iconName = iconFromYourAssets
    }
}
