//
//  KGaugeShape.swift
//  KGaugeView
//
//  Created by Ahmad Eyad on 29/04/2021.
//

import SwiftUI

public struct KGaugeShape: InsettableShape {
    var startAngle: Angle
    var endAngle: Angle
    var clockwise: Bool
    var insetAmount: CGFloat = 0
    
    public func path(in rect: CGRect) -> Path {
        
        var path = Path()
        path.addArc(center: CGPoint(x: rect.midX, y: rect.midY), radius: rect.width / 2 - insetAmount, startAngle: startAngle, endAngle: endAngle, clockwise: false)
        
        return path
    }
    
    public func inset(by amount: CGFloat) -> some InsettableShape {
        var arc = self
        arc.insetAmount += amount
        return arc
    }
}
