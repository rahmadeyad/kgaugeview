# KGaugeView

[![CI Status](https://img.shields.io/travis/rahmadeyadg@gmail.com/KGaugeView.svg?style=flat)](https://travis-ci.org/rahmadeyadg@gmail.com/KGaugeView)
[![Version](https://img.shields.io/cocoapods/v/KGaugeView.svg?style=flat)](https://cocoapods.org/pods/KGaugeView)
[![License](https://img.shields.io/cocoapods/l/KGaugeView.svg?style=flat)](https://cocoapods.org/pods/KGaugeView)
[![Platform](https://img.shields.io/cocoapods/p/KGaugeView.svg?style=flat)](https://cocoapods.org/pods/KGaugeView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KGaugeView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KGaugeView'
```

## Author

rahmadeyadg@gmail.com, a.alrihawi@ucg.ae

## License

KGaugeView is available under the MIT license. See the LICENSE file for more info.
