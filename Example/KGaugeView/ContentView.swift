//
//  ContentView.swift
//  KGaugeView_Example
//
//  Created by Ahmad Eyad on 29/04/2021.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import SwiftUI
import KGaugeView

struct ContentView: View {
    
    // declare view model (eg)
    @ObservedObject var viewModel = KGaugeViewModel()
    // declare list as per the below (you can mix icons from system and your assets folder)
    @State var list: [KGaugeModel] = [
        KGaugeModel(score: 0.4, systemIconName: "bed.double.fill"),
        KGaugeModel(score: 0, systemIconName: "doc.text.below.ecg.fill"),
        KGaugeModel(score: 0.6, systemIconName: "paintbrush.pointed.fill"),
        KGaugeModel(score: 1, iconName: "heart")
    ]
    // declare your each level color as per the below list
    @State var colors: [Color] = [.red,.orange,.yellow,.blue,.green]
    
    var body: some View {
        ScrollView {
            VStack {
                // use the gauge wherever you want and as you want
                KGaugeView(title: "My Mind", list: list, animationDuration: 0.7, size: 200, levelColors: colors)
                KGaugeView(title: "health", list: list)
                KGaugeView(viewModel: viewModel)
                TextField("", text: $viewModel.title)
                    .padding()
                    .background (
                        ZStack {
                            RoundedRectangle(cornerRadius: 5).stroke(Color.black, lineWidth: 1)
                            Text("Gauge Title")
                                .font(Font.system(size: 12))
                                .padding([.top, .leading], 2)
                                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
                        }
                    )
                VStack {
                    Text(String(format: "Total Angle Of The Gauge: %.0f Degrees", viewModel.totalAngle))
                    Slider(value: $viewModel.totalAngle, in: 120...300, step: 1)
                    Text("The Number Of Levels: \(Int(viewModel.levelsCount))")
                    Slider(value: $viewModel.levelsCount, in: 3...9, step: 1)
                    Text("Gauge Size: \(Int(viewModel.size))")
                    Slider(value: $viewModel.size, in: 100...350, step: 1)
                    Text(String(format: "Animation Duration: %.1f Seconds", viewModel.duration))
                    Slider(value: $viewModel.duration, in: 0.3...5, step: 0.1)
                    Text("The Number Of Gauge Elements: \(Int(viewModel.count))")
                    Slider(value: $viewModel.count, in: 3...10, step: 1)
                }
            }
            .padding()
        }
        .padding(.top, UIApplication.shared.windows.first?.safeAreaInsets.top)
        .padding(.bottom, UIApplication.shared.windows.first?.safeAreaInsets.bottom)
        .background(Color.white)
        .edgesIgnoringSafeArea(.all)
    }
}
