//
//  KGaugeViewApp.swift
//  KGaugeView_Example
//
//  Created by Ahmad Eyad on 29/04/2021.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import SwiftUI

@main
struct KGaugeViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onAppear {
                    UIApplication.shared.windows.first?.overrideUserInterfaceStyle = .light
                }
        }
    }
}
